"""Handles domain creation, verification and TLS setup"""
import json
import subprocess


def exists(domain: str) -> bool:
    res = subprocess.run(
        ["yunohost", "domain", "list", "--output-as", "json"],
        text=True,
        capture_output=True,
    )
    if res.returncode != 0:
        print("Error checking domain exists")
        print(res.stderr)
        exit(res.returncode)
    domains_list = json.loads(res.stdout)["domains"]
    return domain in domains_list


def create(domain: str, with_tls: bool = True) -> bool:
    res = subprocess.run(["yunohost", "domain", "add", domain])

    status = res.returncode == 0

    if status and with_tls:
        return setup_letsencrypt(domain)
    else:
        return status


def has_letsencrypt(domain: str) -> bool:
    res = subprocess.run(
        ["yunohost", "domain", "cert-status", "--output-as", "json", "--full"],
        text=True,
        capture_output=True,
    )

    if res.returncode != 0:
        print(f"Error checking certificate for domain {domain}")
        print(res.stderr)
        exit(res.returncode)
    else:
        certificates = json.loads(res.stdout)["certificates"]
        if domain in certificates:
            return certificates[domain]["CA_type"]["code"] == "lets-encrypt"
        else:
            return False


def setup_letsencrypt(domain: str) -> bool:
    if has_letsencrypt(domain):
        return True
    else:
        diag = subprocess.run(
            [
                "yunohost",
                "diagnosis",
                "run",
                "basesystem",
                "ip",
                "dnsrecords",
                "ports",
                "web",
                "--force",
            ],
            text=True,
            capture_output=True,
        )
        if diag.returncode != 0:
            print("Error running diagnosis for dnsrecords and web")
            print(diag.stderr)
            exit(diag.returncode)

        cert = subprocess.run(
            ["yunohost", "domain", "cert-install", domain],
            text=True,
            capture_output=True,
        )
        print(cert.stdout)
        if cert.returncode != 0 or "Error" in cert.stderr:
            print(f"Error installing Let's Encrypt certificate for {domain}")
            print(cert.stderr)
            exit(cert.returncode or 1)
        return True


def create_if_not_exists(domain: str, with_tls: bool = True) -> bool:
    if not exists(domain):
        return create(domain, with_tls)
    else:
        return True
