from typing import Any
from typing import Dict
from typing import List


def last_updated(apps_list: List[Dict[str, Any]]) -> Dict[str, Any]:
    """Returns the last updated app by checking their install and upgrade time"""
    update_time = apps_list[0]["settings"]["install_time"]
    last = apps_list[0]
    for app in apps_list:
        if update_time < app["settings"]["install_time"]:
            last = app
            update_time = app["settings"]["install_time"]
        if (
            "upgrade_time" in app["settings"]
            and update_time < app["settings"]["upgrade_time"]
        ):
            last = app
            update_time = app["settings"]["upgrade_time"]

    return last
