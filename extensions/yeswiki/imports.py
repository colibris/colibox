"""Performs lms imports for the specified courses

The extension excepts his parameters to be in the following form:
```
[
    {
        "importurl": "https://your-yeswiki-instance",
        "key": "a-valid-API-key",
        "courses": ["all"]
    },
    {
        "importurl": "https://another-yeswiki-instance",
        "key": "a-valid-API-key",
        "courses": ["ASpecificCourse", "AnotherCourse"]
    }
]
```

This extension requires the "lms" plugin to be installed first!
"""
import subprocess
from typing import List

from ..utils import run_as_user


def run(application, instances, dryrun):
    if dryrun:
        print(__name__, instances)
    else:
        print("Importing courses")
        final_path = application["settings"]["final_path"]
        ynh_app_id = application["settings"]["id"]
        with run_as_user(ynh_app_id):
            for instance in instances:
                import_lms_course(
                    final_path,
                    instance["importurl"],
                    instance["key"],
                    instance["courses"],
                )


def import_lms_course(
    wiki_path: str, remote_url: str, remote_token: str, courses: List[str]
):
    command = [
        "php7.3",
        "tools/lms/commands/console",
        "lms:import-courses",
        remote_url,
        remote_token,
        "-f",
    ]
    for course in courses:
        command.append("-c")
        command.append(course)

    subprocess.run(list(command), cwd=wiki_path)
