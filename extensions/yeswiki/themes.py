"""Installs the specified YesWiki themes

The themes must be available in the public YesWiki repository

They can be specified as a list of themes, and the latest versions will be used
Or as a table of `"theme": "version"` and the specific versions will be used

Examples:
```json
"themes": {
    "interface": "2017-09-29-2",
    "far": "latest"
}
```

```json
"themes": [
    "interface",
    "far"
]
```
"""
from pathlib import Path
from typing import Union

from . import repository
from ..utils import run_as_user


def run(application: dict, themes: Union[list, dict], dryrun: bool) -> None:
    if dryrun:
        print(__name__, themes)
    else:
        final_path = Path(application["settings"]["final_path"])
        ynh_app_id = application["settings"]["id"]
        with run_as_user(ynh_app_id):
            for name in themes:
                if isinstance(themes, dict):
                    version = themes.get(name, "latest")
                else:
                    version = "latest"
                print(f"Installing theme {name}, version {version}")
                repository.download(final_path, repository.Types.THEME, name, version)
